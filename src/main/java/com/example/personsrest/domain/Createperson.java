package com.example.personsrest.domain;

import lombok.Value;

@Value
public class Createperson {
    String name;
    String city;
    int age;
}
