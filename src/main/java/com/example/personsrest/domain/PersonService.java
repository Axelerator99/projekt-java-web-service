package com.example.personsrest.domain;

import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PersonService {

    PersonRepository personRepository;
    GroupRemote groupRemote;

    public Stream<Person> all() {
        return personRepository.findAll().stream();
    }

    public Stream<Person> findAll(String search, int pagenumber, int pagesize) {
        Pageable sortedByName = PageRequest.of(pagenumber,pagesize, Sort.by(search));
        return personRepository.findAllByNameContainingOrCityContaining(search, search, sortedByName).stream();
    }

    public Person get(String id) throws PersonNotFoundException {
        return  personRepository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException(id));
    }

    public Person create(String name, String city, int age) {
        Person person = new PersonEntity(UUID.randomUUID().toString(),name,age,city, new ArrayList<>(),true);
        return  personRepository.save(person);
    }

    public Person update(String id, String name, String city, int age) throws PersonNotFoundException {
        Person person = personRepository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException(id));
        person.setName(name);
        person.setAge(age);
        person.setCity(city);
        return personRepository.save(person);
    }

    public void delete(String id) {
        personRepository.delete(id);
    }

    public Person addGroup(String id, String groupName) throws PersonNotFoundException {
        Person person = personRepository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException(id));
        String groupId = groupRemote.createGroup(groupName);
        person.addGroup(groupId);
        return personRepository.save(person);
    }

    public Person deleteGroup(String id, String groupName) throws PersonNotFoundException {
        Person person = personRepository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException(id));
        String selectedGroupId = person.getGroups().stream()
                .filter(groupId -> groupRemote.getNameById(groupId).equals(groupName))
                .findAny()
                .orElse(null);
        person.removeGroup(selectedGroupId);
        return personRepository.save(person);
    }
}
