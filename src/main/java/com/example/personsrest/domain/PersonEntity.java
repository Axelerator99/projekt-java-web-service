package com.example.personsrest.domain;

import com.example.personsrest.remote.GroupRemote;
import com.example.personsrest.remote.GroupRemoteImpl;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
public class PersonEntity implements Person {

    String id;
    String name;
    int age;
    String city;
    List<String> groups;
    boolean active;

    public PersonEntity(String id, String name, int age, String city, List<String> groups, boolean active) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.city = city;
        this.groups = groups;
        this.active = active;
    }

    @Override
    public boolean isActive() {
        return false;
    }


    @Override
    public void addGroup(String groupId) {
        groups.add(groupId);
    }

    @Override
    public void removeGroup(String groupId) {
        groups.remove(groupId);
    }
}
