package com.example.personsrest.domain;
import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
@AllArgsConstructor
public class PersonController {
    PersonService personService;

    @GetMapping
    public List<PersonDTO> all() {
        return personService.all()
                .map(PersonController::toDTO)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "", params = {"search","pagenumber","pagesize"}, method = RequestMethod.GET)
    public @ResponseBody List<PersonDTO> all(@RequestParam("search") String search, @RequestParam("pagenumber") int pagenumber, @RequestParam("pagesize") int pagesize) {
        return personService.findAll(search, pagenumber, pagesize)
                .map(PersonController::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(toDTO(personService.get(id)));
        } catch (PersonNotFoundException e){
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public PersonDTO create(@RequestBody Createperson createperson){
        return toDTO(personService.create(createperson.getName(),createperson.getCity(),createperson.getAge()));
    }

    @PutMapping("/{id}")
    public PersonDTO update(@RequestBody UpdatePerson updatePerson, @PathVariable("id") String id) throws PersonNotFoundException {
        return toDTO(personService.update(id, updatePerson.getName(), updatePerson.getCity(), updatePerson.getAge())
        );
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        personService.delete(id);
    }

    @PutMapping("/{personId}/addGroup/{groupName}")
    public PersonDTO addGroup(@PathVariable("personId") String id, @PathVariable("groupName") String groupName) throws PersonNotFoundException {
        return toDTOGroup(personService.addGroup(id, groupName));
    }

    @DeleteMapping("/{personId}/removeGroup/{groupName}")
    public PersonDTO deleteGroup(@PathVariable("personId") String id, @PathVariable("groupName") String groupName) throws PersonNotFoundException {
        return toDTO(personService.deleteGroup(id, groupName));
    }

    private static PersonDTO toDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getAge(),
                person.getCity(),
                person.getGroups(),
                person.isActive()
        );
    }

    GroupRemote groupRemote;
    private PersonDTO toDTOGroup(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getAge(),
                person.getCity(),
                Collections.singletonList(groupRemote.getNameById(person.getGroups().get(0))),
                person.isActive()
        );
    }
}

