package com.example.personsrest.remote;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;


public class GroupRemoteImpl implements GroupRemote {
    WebClient webClient;

    public GroupRemoteImpl() {
        webClient = WebClient.builder()
                //.baseUrl("/api/groups/")
                .build();
    }

    @Override
    public String getNameById(String groupId) {
        return webClient.get().uri("https://groups.edu.sensera.se/api/groups/" + groupId)
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer " + keyCloakToken.getAccessToken())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Group.class)
                .block().getName();
    }

    KeyCloakToken keyCloakToken = KeyCloakToken.acquire("https://iam.sensera.se/", "test", "group-api", "user", "djnJnPf7VCQvp3Fc")
            .block();
    @Override
    public String createGroup(String name) {
         return webClient.post()
                .uri("https://groups.edu.sensera.se/api/groups")
                .header("Authorization", "Bearer " + keyCloakToken.getAccessToken())
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(new CreateGroup(name)))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Group.class)
                .block().getId();
    }

    @Override
    public String removeGroup(String name) {
        return null;
    }

    @Value
    static class CreateGroup {
        String name;
    }

    @Value
    static class Group {
        String id;
        String name;

        @JsonCreator
        public Group(
                @JsonProperty("id") String id,
                @JsonProperty("name") String name) {
            this.id = id;
            this.name = name;
        }
    }


}
